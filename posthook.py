import urllib
import webapp2
import json
from google.appengine.api import urlfetch


class Listener(webapp2.RequestHandler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello World!')

    def post(self):
        google_code_payload = json.loads(self.request.body)
        url = "https://taskpanda.slack.com/services/hooks/incoming-webhook?token=GNzRU9FRj6tGRMjxtnag5BLe"

        data_fields = {
            "channel": "#taskpanda",
            "username": "pandabot",
        }

        msg = "Code pushed in %s!\n" % google_code_payload["project_name"]

        for revision in google_code_payload["revisions"]:
            msg += "By :\tbro %s\n" % revision["author"]
            msg += "Msg:\t%s\n" % revision["message"]
            hash = revision["url"].split("/")[-2]
            msg += "<%s>\n" % ("https://code.google.com/p/cs2103jan2014-w15-1j/source/detail?r=" + hash)

        data_fields["text"] = msg

        data = json.dumps(data_fields)
        result = urlfetch.fetch(url=url,
                                payload=data,
                                method=urlfetch.POST,
                                headers={'Content-Type:': 'application/json'})

application = webapp2.WSGIApplication([
    ('/', Listener),
], debug=True)
